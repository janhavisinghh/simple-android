buildscript {
  ext.versions = [
      minSdk              : 21,
      compileSdk          : 29,
      kotlin              : '1.3.61',
      supportLib          : '1.0.0',
      recyclerView        : '1.0.0',
      material            : '1.0.0',
      cardview            : '1.0.0',
      room                : '2.1.0',
      supportTest         : '1.1.1',
      timber              : '4.7.0',
      dagger              : '2.24',
      butterKnife         : '8.8.1',
      kotterKnife         : 'e157638df1',
      coreTesting         : '2.0.0',
      moshi               : '1.8.0',
      retrofit            : '2.4.0',
      stetho              : '1.5.0',
      sentry              : '1.7.22',
      slf4j               : '1.7.25',
      groupie             : '2.3.0',
      flow                : '1.0.0-alpha3',
      rxJava              : '2.1.9',
      rxAndroid           : '2.0.2',
      rxBinding           : '2.1.1',
      rxKotlin            : '2.2.0',
      rxPreference        : '2.0.0',
      qrReader            : '2.1.2',
      picasso             : '2.71828',
      jcabiXml            : '0.21.4',
      threeTenBp          : '1.3.6',
      lazyThreeTenBp      : '0.3.0',
      okLoggingInterceptor: '3.10.0',
      traceur             : '1.0.1',
      maskedEditText      : '1.0.5',
      itemAnimators       : '1.0.2',
      jbcrypt             : '0.3m',
      workManager         : '2.2.0',
      faker               : '1.2.7',
      truth               : '0.40',
      junit               : '4.12',
      mockito             : '2.18.3',
      mockitoKotlin       : '1.5.0',
      junitParams         : '1.1.1',
      sqliteAndroid       : '3.24.0',
      playServicesAuth    : '16.0.1',
      playServicesVision  : '16.2.0',
      viewPump            : '2.0.3',
      pinEntryEditText    : '2.0.6',
      fbShimmer           : '0.3.0',
      javaStringSimilarity: '1.1.0',
      espresso            : '3.1.1',
      leakCanary          : '1.6.2',
      constraintLayout    : '1.1.3',
      playServicesLocation: '16.0.0',
      codeScanner         : '2.1.0',
      firebaseCore        : '16.0.8',
      firebaseConfig      : '16.4.1',
      googleServices      : '4.0.1',
      playCore            : '1.6.1',
      fastScroll          : '2.0.0',
      flexbox             : '1.1.0',
      mixpanel            : '5.6.4',
      mobius              : '1.3.0',
      guava               : '28.1-jre', // Used by the 'mobius-migration' library ONLY in tests
      uuidGenerator       : '3.2.0',
      assistedInject      : '0.5.2'
  ]

  repositories {
    google()
    mavenCentral()
    maven { url 'https://jitpack.io' }
    jcenter()
  }

  dependencies {
    classpath 'com.android.tools.build:gradle:3.5.2'
    classpath "org.jetbrains.kotlin:kotlin-gradle-plugin:$versions.kotlin"
    classpath "io.sentry:sentry-android-gradle-plugin:$versions.sentry"
    classpath "com.google.gms:google-services:$versions.googleServices"
  }
}

allprojects {
  repositories {
    google()
    mavenCentral()
    jcenter()
  }
}

task clean(type: Delete) {
  delete rootProject.buildDir
}

task compileSpellfix(type: Copy, dependsOn: 'spellfix:externalNativeBuildRelease') {
  doLast {
    logger.lifecycle("Copied libspellfix native libs to app jniLibs!")
  }
  from 'libspellfix/build/intermediates/ndkBuild/release'
  into 'app/src/main/jniLibs'
  eachFile {
    if (!name.endsWith('.so')) {
      exclude()
    } else {
      def pathParts = it.path.split("/").reverse()
      // We expect the 0th item to be the native lib name and the 1st item to be the abi
      if (pathParts.length < 2) {
        logger.log(LogLevel.ERROR, "Unrecognized native lib path: $it.path")
        throw TaskExecutionException(this)
      }

      def libName = pathParts[0]
      def libAbi = pathParts[1]
      it.path = "$libAbi/$libName"
      logger.lifecycle("Copy native lib: $it.sourcePath to: $it.path")
    }
  }
  includeEmptyDirs false
}
